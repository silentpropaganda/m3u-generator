# encoding: UTF-8
def write
	results = Dir.entries(Dir.pwd).select {|f| @files.any? {|type| f.end_with? type}}
	if results.empty?
		results =  Dir['**/*'].reject {|f| File.directory?(f) }.select {|f| @files.any? {|type| f.end_with? type}}		
	end
	if !results.empty?
		File.open(File.basename(Dir.getwd) + ".m3u", "w") do |z|
			results.each do |x|
				z.puts x
			end
		end
	end
end

ARGV.each do |x|
	case 
	when x.start_with?('type=')				
		@files = x.dup
		@files.slice!("type=")
		@files = @files.split(',')
	when x.start_with?('path=')
		@path = x.dup
		@path.slice!("path=")
		Dir.chdir(@path)
	end
	
end
if !@files
	@files = ['mp3','flac','ogg']
end
if !@path 
	@path = Dir.pwd 
end


write
x = Dir['**/'] 
x.each do |dir|
	Dir.chdir(dir)
	write
	Dir.chdir(@path)
end
